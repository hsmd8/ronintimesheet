<?php
/*
Plugin Name: Timesheet
Author: Sylwia Gawrońska
Version: 1.0
Text Domain: timesheet
*/

global $wpdb;

defined('ABSPATH') || exit;

define('ts_path', plugin_dir_path(__FILE__));

define("LABEL_LOGIN_ERROR", __( 'Błąd logowania. Spróbuj ponownie', 'timesheet' ));
define("LABEL_NO_ACTIVE_TIMESHEETS", __( 'Nie posiadasz aktywnych timesheetow', 'timesheet' ));
define("LABEL_TIMESHEET_ALREADY_EXISTS", __( 'Ten użytkownik posiada już timesheet na podany okres', 'timesheet' ));
define("LABEL_MONTH", __( 'Miesiąc', 'timesheet' ));
define("LABEL_YEAR", __( 'Rok', 'timesheet' ));
define("LABEL_HOURS_COUNT", __( 'Ilość godzin', 'timesheet' ));
define("LABEL_DATE", __( 'Data', 'timesheet' ));
define("LABEL_PROJECT", __( 'Projekt', 'timesheet' ));
define("LABEL_LOCKED", __( 'Przesłane', 'timesheet' ));
define("LABEL_PROCESSED", __( 'Zaakceptowane', 'timesheet' ));
define("LABEL_EDIT", __( 'Edytuj', 'timesheet' ));
define("LABEL_WORKER", __( 'Pracownik', 'timesheet' ));
define("LABEL_ID", __( 'ID', 'timesheet' ));
define("LABEL_DETAILS", __( 'Szczegóły', 'timesheet' ));
define("LABEL_ACCEPT", __( 'Akceptuj', 'timesheet' ));
define("LABEL_UNDO", __( 'Confij', 'timesheet' ));
define("LABEL_DONE", __( 'Ukończone', 'timesheet' ));
define("LABEL_REJECT", __( 'Odrzuć', 'timesheet' ));
define("LABEL_NOT_DONE", __( 'Nie ukończone', 'timesheet' ));
define("LABEL_YES", __( 'Tak', 'timesheet' ));
define("LABEL_NO", __( 'Nie', 'timesheet' ));
define("LABEL_ADD", __( 'Dodaj', 'timesheet' ));
define("LABEL_FILTER", __( 'Filtruj', 'timesheet' ));
define("LABEL_HELLO", __( 'Witaj', 'timesheet' ));
define("LABEL_TOTAL", __( 'Łącznie', 'timesheet' ));
define("LABEL_NOPERMISSION", __( 'Nie masz uprawnień aby wykonać tę czynność', 'timesheet' ));
define("LABEL_SAVE", __( 'Zapisz', 'timesheet' ));
define("LABEL_SEND", __( 'Zapisz i wyślij', 'timesheet' ));
define("LABEL_ERROR", __('Wystąpił błąd zapisu', 'timesheet'));
define("LABEL_SAVE_SUCCESS", __("Aktualizacja przebiegła pomyślnie"));

define("LABEL_FROM", __('W okresie od', 'timesheet'));
define("LABEL_TO", __('do', 'timesheet'));

define("STATUS_SUCCESS", 'SUCCESS');
define("STATUS_FAIL", 'FAIL');

define("ACTION_LOCK", 'LOCK');
define("ACTION_UNLOCK", 'UNLOCK');
define("ACTION_APPROVE", 'APPROVE');
define("ACTION_UNAPPROVE", 'UNAPPROVE');


add_action('admin_init', 'ts_do_enqueue_scripts');
add_action('init', 'ts_do_enqueue_scripts');

function ts_do_enqueue_scripts()
{

    wp_enqueue_script('timesheet', plugins_url('/js/admin.js', __FILE__), array('jquery', 'jquery-ui-core'));
    wp_enqueue_script('datatables', plugins_url('/vendor/datatables.min.js', __FILE__), array('jquery'));

    wp_enqueue_script('pdfmake', plugins_url('/vendor/pdfmake.min.js', __FILE__), array('jquery'));
    wp_enqueue_script('vfsfonts', plugins_url('/vendor/vfs_fonts.js', __FILE__), array('jquery'));

    wp_register_style('datatables-css', plugins_url('/vendor/datatables.min.css', __FILE__));
    wp_enqueue_style('datatables-css');

    wp_register_style('ts-styles', plugins_url('/css/style.css', __FILE__));
    wp_enqueue_style('ts-styles');

    $phpData = array(
        'apiSettings' => array(
            'root' => esc_url_raw(get_rest_url()),
            'namespace' => 'timesheet/v2',
            'nonce' => (wp_installing() && !is_multisite()) ? '' : wp_create_nonce('wp_rest')
        ),
        'actions' => array(
            'ACTION_LOCK' => ACTION_LOCK,
            'ACTION_UNLOCK' => ACTION_UNLOCK,
            'ACTION_APPROVE' => ACTION_APPROVE,
            'ACTION_UNAPPROVE' => ACTION_UNAPPROVE
        ),
        'status' => array(
            'STATUS_SUCCESS' => STATUS_SUCCESS,
            'STATUS_FAIL' => STATUS_FAIL
        ),
        'labels' => array(
            'LABEL_NO_ACTIVE_TIMESHEETS' => LABEL_NO_ACTIVE_TIMESHEETS,
            'LABEL_MONTH' => LABEL_MONTH,
            'LABEL_YEAR' => LABEL_YEAR,
            'LABEL_HOURS_COUNT' => LABEL_HOURS_COUNT,
            'LABEL_LOCKED' => LABEL_LOCKED,
            'LABEL_PROCESSED' => LABEL_PROCESSED,
            'LABEL_EDIT' => LABEL_EDIT,
            'LABEL_WORKER' => LABEL_WORKER,
            'LABEL_ID' => LABEL_ID,
            'LABEL_DETAILS' => LABEL_DETAILS,
            'LABEL_ACCEPT' => LABEL_ACCEPT,
            'LABEL_UNDO' => LABEL_UNDO,
            'LABEL_DONE' => LABEL_DONE,
            'LABEL_REJECT' => LABEL_REJECT,
            'LABEL_NOT_DONE' => LABEL_NOT_DONE,
            'LABEL_YES' => LABEL_YES,
            'LABEL_NO' => LABEL_NO,
            'LABEL_ADD' => LABEL_ADD,
            'LABEL_FILTER' => LABEL_FILTER,
            'LABEL_NOPERMISSION' => LABEL_NOPERMISSION,
            'LABEL_DATE' => LABEL_DATE,
            'LABEL_PROJECT' => LABEL_PROJECT,
            'LABEL_TOTAL' => LABEL_TOTAL,
            'LABEL_SAVE' => LABEL_SAVE,
            'LABEL_SEND' => LABEL_SEND,
            'LABEL_ERROR' => LABEL_ERROR,
            'LABEL_SAVE_SUCCESS' => LABEL_SAVE_SUCCESS,
            'LABEL_FROM' => LABEL_FROM,
            'LABEL_TO' => LABEL_TO
        ),
    );


    wp_localize_script('timesheet', 'phpData', $phpData);

}


register_activation_hook(__FILE__, 'ts_activate');
register_activation_hook(__FILE__, 'ts_flush_rewrite_rules');
register_activation_hook(__FILE__, 'TS_Install');

// Flush rewrite rules
function ts_flush_rewrite_rules() {
    flush_rewrite_rules();
}

function ts_activate()
{
    add_role('worker', 'Pracownik',
        array(
            'read' => true));
}

add_action('admin_menu', 'ts_timesheet_menu');

function ts_timesheet_menu()
{
    $complete_url = wp_nonce_url($bare_url, 'edit-menu_' . $comment_id, 'TS_Nonce');
    add_menu_page('Admin Menu', __('Timesheet', 'timesheet'), 'manage_options', 'TS_Timesheet' . $complete_url, 'List_All_Timesheets', 'dashicons-calendar-alt');
    add_submenu_page('TS_Timesheet' . $complete_url, 'Admin Menu', __('Dodaj nowy', 'timesheet'), 'manage_options', 'TS_Timesheet_New' . $complete_url, 'Add_New_Timesheet');
}


function List_All_Timesheets()
{
    require_once(dirname(__FILE__) . '/templates/ts-timesheet-list.php');
}

function Add_New_Timesheet()
{
    require_once(dirname(__FILE__) . '/templates/ts-timesheet-new.php');
}

function TS_Install()
{
    require_once(dirname(__FILE__) . '/core/ts-install.php');
}


require_once ts_path . 'ts-init.php';