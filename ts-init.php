<?php


class TS_API
{
    function __construct()
    {

        add_action('init', array(&$this, 'init'), 0);

    }

    function init()
    {

        require_once ts_path . 'core/ts-api.php';
        require_once ts_path . 'core/ts-utilities.php';
        require_once ts_path . 'core/ts-pracownik.php';

        require_once ts_path . 'core/ts-roles.php';
        require_once ts_path . 'core/ts-shortcodes.php';
        require_once ts_path . 'core/ts-templates.php';
        require_once ts_path . 'core/ts-credentials.php';
        require_once ts_path . 'core/ts-timesheet.php';


        $this->roles = new TS_ROLES();
        $this->shortcodes = new TS_Shortcodes();
        $this->templates = new TS_Templates();
        $this->credentials = new TS_Credentials();
        $this->timesheet = new TS_Timesheet();
    }


}


global $timesheet;

$timesheet = new TS_API();