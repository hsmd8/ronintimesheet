<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class TS_Credentials

{

    function __construct() {

        add_action( 'ts_check_credentials', array( &$this, 'ts_restrict_user_access') );

    }


    function ts_restrict_user_access() {

        $type = get_post_type();

        if ( $type === 'ts-worker' ) {

            // Set redirect to true by default
            $redirect = true;

            // If logged in do not redirect
            if ( is_user_logged_in() ) {

                $user = wp_get_current_user();

                if ( in_array( 'worker', (array) $user->roles ) || in_array( 'administrator', (array) $user->roles )) {
                    $redirect = false;
                }
            }

            // Redirect people without access to homepage
            if ( $redirect ) {

                $current_url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                wp_redirect( esc_url( home_url( '/login' ) . '?to='.$current_url ), 302 );
                exit;
           }

        }

    }

}