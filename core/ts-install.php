<?php
if(!current_user_can('manage_options'))
{
    die('Access Denied');
}

global $wpdb;

$table_name  = $wpdb->prefix . "ts_timesheets";

$days = '';

for ($i = 1; $i <= 31; $i++) {
    $days .= 'TS_Date'.$i.' DATE NULL,';
    $days .= 'TS_Time'.$i.' INT NULL,';
    $days .= 'TS_Task'.$i.' VARCHAR(255) NULL,';
}

$sql = 'CREATE TABLE IF NOT EXISTS ' .$table_name . '(
		id INTEGER(10) UNSIGNED AUTO_INCREMENT,
        TS_Month VARCHAR(255) NOT NULL,
		TS_Year VARCHAR(255) NOT NULL,
        TS_User VARCHAR(255) NOT NULL,
        TS_Locked BOOLEAN NOT NULL,
        TS_Processed BOOLEAN NOT NULL,
		' .$days . '
		PRIMARY KEY (id))';

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);


$sqla   = 'ALTER TABLE ' . $table_name . ' CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';

$wpdb->query($sqla);