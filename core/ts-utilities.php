<?php

add_action( 'ts_build_years_menu', 'ts_build_years_menu_callback');

function ts_build_years_menu_callback() {

    $output = '';

    $cutoffYear = 2017;
    $currentYear = date('Y');

    $output .= '<select name="year">';
    $output .= '<option value="">---------------------</option>';

    for ($y=$currentYear; $y>=$cutoffYear; $y--) {
        $selected = ($currentYear == $y) ? 'selected' : '';
        $output .= '<option '. $selected .' value="' . $y . '">' . $y . '</option>';
    }

    $output .= '</select>';

    echo $output;
}


add_action( 'ts_build_months_menu', 'ts_build_months_menu_callback');

function ts_build_months_menu_callback() {

    $output = '';

    $currentMonth = date('n');

    $output .= '<select name="month">';
    $output .= '<option value="">---------------------</option>';

    for ($m=1; $m<=12; $m++) {
        $selected = ($currentMonth == $m) ? 'selected' : '';
        $output .= '<option '. $selected .' value="' . $m . '">' . date_i18n('F', mktime(0,0,0,$m)) . '</option>';
    }

    $output .= '</select>';

    echo $output;
}


add_action( 'ts_build_users_menu', 'ts_build_users_menu_callback');

function ts_build_users_menu_callback() {

    $output = '';

    $users = get_users('role=worker');

    if (count($users) > 0) {

        $output .= '<select name="user">';
        $output .= '<option value="">---------------------</option>';
        foreach ($users as $user) {
            $output .= '<option value="' . $user->ID . '">' . $user->display_name . '</option>';
        }
        $output .= '</select>';

    }

    echo $output;

}

function locked($id) {
    return LABEL_DONE . ' (<i class="timesheetSet" data-item="'.$id.'" data-action="'.ACTION_UNLOCK.'">'.LABEL_REJECT.'</i>)';
}

function doApprove($id) {
    return '<i class="timesheetSet" data-item="'.$id.'" data-action="'.ACTION_APPROVE.'">'.LABEL_ACCEPT.'</i>';
}

function alreadyApproved($id) {
    return LABEL_PROCESSED . ' (<i class="timesheetSet" data-item="'.$id.'" data-action="'.ACTION_UNAPPROVE.'">'.LABEL_UNDO.'</i>)';
}