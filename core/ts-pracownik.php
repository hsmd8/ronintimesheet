<?php

add_action( 'ts_timesheet_list', 'ts_timesheet_list_callback');

function ts_timesheet_list_callback() {

    global $wpdb;

    $output = "";
    $table_name = $wpdb->prefix . "ts_timesheets";

    $results=$wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE TS_USER LIKE %s",wp_get_current_user()->ID));

    if (count($results) > 0) {

        $output .= '<div id="timesheet-details"></div>';

        $output .= '<table id="listResults" class="table table-striped table-bordered" cellspacing="0" width="100%">';

        $output .= '<thead><tr><td>'.LABEL_MONTH.'</td><td>'.LABEL_YEAR.'</td><td>'.LABEL_HOURS_COUNT.'</td>
                    <td>'.LABEL_LOCKED.'</td><td>'.LABEL_PROCESSED.'</td><td>'.LABEL_EDIT.'</td></tr></thead>';

        $output .= '<tbody>';


        foreach ($results as $result) {

            $edit = '<i class="timesheetDetails" data-level="0" data-item="'.$result->id.'">'.LABEL_EDIT.'</i>';

            $total = 0;

            for ($i = 1; $i <= 31; $i++) {
                $total += $result->{'TS_Time'.$i};
            }

            $locked = $result->TS_Locked ? LABEL_YES : LABEL_NO;
            $processed = $result->TS_Processed ? LABEL_YES : LABEL_NO;

            $output .= '<tr>';

            $output .= '<td>'. date_i18n('F', mktime(0, 0, 0, $result->TS_Month, 10));'</td>';
            $output .= '<td>'.$result->TS_Year.'</td>';
            $output .= '<td>'.$total.'</td>';
            $output .= '<td>'. $locked .'</td>';
            $output .= '<td>'.$processed.'</td>';
            $output .= '<td>'.$edit.'</td>';

            $output .= '</tr>';

        }

        $output .= '</tbody>';
        $output .= '</table>';


    } else {
        $output .= LABEL_NO_ACTIVE_TIMESHEETS;
    }

    echo $output;


}