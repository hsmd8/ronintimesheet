<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class TS_Templates

{

    protected $templates;

    function __construct() {


        $this->templates = array();

        $this->templates = array(

            array(
                'slug' => 'ts-worker',
                'template' => 'templates/ts-pracownik.php',
                'name' =>  __('Strony Pracowników', 'timesheet'),
                'singular_name' => __('Strona Pracownika', 'timesheet'),
                'add_new' => __('Dodaj nową stronę pracownika', 'timesheet'),
                'edit_item' => __('Edytuj stronę', 'timesheet'),
                'new_item' => __('Nowa strona', 'timesheet'),
                'add_new_item' => __('Dodaj nową stronę', 'timesheet'),
                'view_item' => __('Zobacz stronę', 'timesheet'),
                'not_found' => __('Brak wyników', 'timesheet')
            )
        );

        add_action( 'init', array(&$this, 'ts_custom_post_types') );

        add_action("template_redirect", array(&$this, 'ts_template_redirect') );

    }





    function ts_custom_post_types() {


        foreach ($this->templates as $template) {



            $labels = array(
                'name' => $template["name"],
                'singular_name' => $template["singular_name"],
                'add_new' => $template["add_new"],
                'edit_item' => $template["edit_item"],
                'new_item' => $template["new_item"],
                'add_new_item' => $template["add_new_item"],
                'view_item' => $template["view_item"],
                'not_found' => $template["not_found"],
            );

            $args = array(
                'labels' => $labels,
                'public' => true,
                'show_ui' => true,
                'capability_type' => 'post',
                'hierarchical' => false,
                'rewrite' => array('slug' => $template["slug"]),
                'supports' => array('title', 'editor')
            );

            register_post_type($template["slug"], $args);
        }

    }

    function ts_template_redirect() {

        global $wp, $post, $wp_query;

        foreach ($this->templates as $template) {

            $templateUrl = ts_path . $template["template"];

            if ($wp->query_vars["post_type"] == $template["slug"]) {

                if (have_posts()) {
                    include($templateUrl);
                    die();
                } else {
                    $wp_query->is_404 = true;
                }

            }

        }

    }


}



