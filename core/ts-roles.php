<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class TS_ROLES {

    function __construct() {

        register_activation_hook( __FILE__, array(&$this, 'add_roles_on_plugin_activation') );
        add_action('admin_menu', array(&$this,  'ts_remove_dashboard') );

    }


    function add_roles_on_plugin_activation() {
        add_role( 'worker', 'Pracownik', array( 'read' => true, 'level_0' => true ) );
    }


    /* Remove the "Dashboard" from the admin menu for non-admin users */
    function ts_remove_dashboard () {
        global $menu;
        $user = wp_get_current_user();

        if( ! in_array( 'administrator', $user->roles ) ) {
            reset( $menu );
            $page = key( $menu );
            while( ( __( 'Dashboard' ) != $menu[$page][0] ) && next( $menu ) ) {
                $page = key( $menu );
            }
            if( __( 'Dashboard' ) == $menu[$page][0] ) {
                unset( $menu[$page] );
            }
            reset($menu);
            $page = key($menu);
            while ( ! $user->has_cap( $menu[$page][1] ) && next( $menu ) ) {
                $page = key( $menu );
            }
            if ( preg_match( '#wp-admin/?(index.php)?$#', $_SERVER['REQUEST_URI'] ) &&
                ( 'index.php' != $menu[$page][2] ) ) {
                wp_redirect( get_option( 'siteurl' ) . '/wp-admin/edit.php');
            }
        }
    }


}