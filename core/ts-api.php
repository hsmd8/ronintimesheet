<?php

add_action('rest_api_init', 'ts_rest_api_init');

function ts_rest_api_init()
{
    $namespace = 'timesheet/v2';

    register_rest_route($namespace,
        '/timesheet-list',
        array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => 'ts_rest_get_timesheets',
            ),
        )
    );

    register_rest_route($namespace,
        '/timesheet-details',
        array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => 'ts_rest_ts_details',
            ),
        )
    );

    register_rest_route($namespace,
        '/timesheet-update',
        array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => 'ts_rest_ts_update',
            ),
        )
    );

    register_rest_route($namespace,
        '/timesheet-locker',
        array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => 'ts_rest_ts_locker',
            ),
        )
    );

}

function ts_rest_get_timesheets(WP_REST_Request $request)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "ts_timesheets";


    $newResults = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT TS_User, TS_User, id, TS_Month, TS_Year, TS_Processed, TS_Locked
              FROM $table_name WHERE id>%d AND TS_USER LIKE %s AND TS_YEAR LIKE %s AND TS_MONTH LIKE %s",
            0, $request->get_param('user') !== '' ? $request->get_param('user') : '%%' , $request->get_param('year') !== '' ? $request->get_param('year') : '%%' . '%', $request->get_param('month') !== '' ? $request->get_param('month') : '%%'));

    foreach ($newResults as $result) {

        $result->TS_Month = date_i18n('F', mktime(0, 0, 0, $result->TS_Month, 10));
        $result->TS_User = get_userdata($result->TS_User)->display_name;

    }

    return rest_ensure_response($newResults);
}


function ts_rest_ts_details(WP_REST_Request $request)
{

    global $wpdb;

    $user = wp_get_current_user();


    $table_name = $wpdb->prefix . "ts_timesheets";
    $newResults = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", intval($request->get_param('id'))));

    if ($user->ID == $newResults[0]->TS_User || in_array('administrator', (array)$user->roles)) {

        foreach ($newResults as $result) {

            $result->TS_User = get_userdata($result->TS_User)->display_name;
        }

        return rest_ensure_response($newResults);
    }

    return LABEL_NOPERMISSION;
}


function ts_rest_ts_locker(WP_REST_Request $request)
{

    global $wpdb;

    $result = false;

    $table_name = $wpdb->prefix . "ts_timesheets";

    $user = wp_get_current_user();

    if (in_array('administrator', (array)$user->roles)) {
        switch ($request->get_param('action')) {
            case ACTION_LOCK:
                $result = $wpdb->query($wpdb->prepare("UPDATE $table_name set TS_Locked=%s WHERE id=%d", 1, intval($request->get_param('id'))));
                break;
            case ACTION_UNLOCK:
                $result = $wpdb->query($wpdb->prepare("UPDATE $table_name set TS_Locked=%s, TS_Processed=%s WHERE id=%d", 0, 0, intval($request->get_param('id'))));
                break;
            case ACTION_APPROVE:
                $result = $wpdb->query($wpdb->prepare("UPDATE $table_name set TS_Processed=%s WHERE id=%d", 1, intval($request->get_param('id'))));
                break;
            case ACTION_UNAPPROVE:
                $result = $wpdb->query($wpdb->prepare("UPDATE $table_name set TS_Processed=%s WHERE id=%d", 0, intval($request->get_param('id'))));
                break;
        }
    }

    $result !== false ? $status = STATUS_SUCCESS : $status = STATUS_FAIL;

    return rest_ensure_response($status);
}


function ts_rest_ts_update(WP_REST_Request $request)
{

    global $wpdb;

    $result = false;

    $user = wp_get_current_user();

    $table_name = $wpdb->prefix . "ts_timesheets";

    $newResults = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", intval($request->get_param('id'))));

    if ($user->ID == $newResults[0]->TS_User || in_array('administrator', (array)$user->roles)) {

        $params = $request->get_params();

        $result = $wpdb->query($wpdb->prepare("UPDATE $table_name set
         TS_Locked=%d,
         TS_Time1=%d,
         TS_Task1=%s,
         TS_Time2=%d,
         TS_Task2=%s,
         TS_Time3=%d,
         TS_Task3=%s,
         TS_Time4=%d,
         TS_Task4=%s,
         TS_Time5=%d,
         TS_Task5=%s,
         TS_Time6=%d,
         TS_Task6=%s,
         TS_Time7=%d,
         TS_Task7=%s,
         TS_Time8=%d,
         TS_Task8=%s,
         TS_Time9=%d,
         TS_Task9=%s,
         TS_Time10=%d,
         TS_Task10=%s,
         TS_Time11=%d,
         TS_Task11=%s,
         TS_Time12=%d,
         TS_Task12=%s,
         TS_Time13=%d,
         TS_Task13=%s,
         TS_Time14=%d,
         TS_Task14=%s,
         TS_Time15=%d,
         TS_Task15=%s,
         TS_Time16=%d,
         TS_Task16=%s,
         TS_Time17=%d,
         TS_Task17=%s,
         TS_Time18=%d,
         TS_Task18=%s,
         TS_Time19=%d,
         TS_Task19=%s,
         TS_Time20=%d,
         TS_Task20=%s,
         TS_Time21=%d,
         TS_Task21=%s,
        TS_Time22=%d,
        TS_Task22=%s,
        TS_Time23=%d,
        TS_Task23=%s,
        TS_Time24=%d,
        TS_Task24=%s,
        TS_Time25=%d,
        TS_Task25=%s,
        TS_Time26=%d,
        TS_Task26=%s,
        TS_Time27=%d,
        TS_Task27=%s,
        TS_Time28=%d,
        TS_Task28=%s,
        TS_Time29=%d,
        TS_Task29=%s,
        TS_Time30=%d,
        TS_Task30=%s,
        TS_Time31=%d,
        TS_Task31=%s

         WHERE id=%d",
            $params["lock"],
            $params["TS_Time1"],
            $params["TS_Task1"],
            $params["TS_Time2"],
            $params["TS_Task2"],
            $params["TS_Time3"],
            $params["TS_Task3"],
            $params["TS_Time4"],
            $params["TS_Task4"],
            $params["TS_Time5"],
            $params["TS_Task5"],
            $params["TS_Time6"],
            $params["TS_Task6"],
            $params["TS_Time7"],
            $params["TS_Task7"],
            $params["TS_Time8"],
            $params["TS_Task8"],
            $params["TS_Time9"],
            $params["TS_Task9"],
            $params["TS_Time10"],
            $params["TS_Task10"],
            $params["TS_Time11"],
            $params["TS_Task11"],
            $params["TS_Time12"],
            $params["TS_Task12"],
            $params["TS_Time13"],
            $params["TS_Task13"],
            $params["TS_Time14"],
            $params["TS_Task14"],
            $params["TS_Time15"],
            $params["TS_Task15"],
            $params["TS_Time16"],
            $params["TS_Task16"],
            $params["TS_Time17"],
            $params["TS_Task17"],
            $params["TS_Time18"],
            $params["TS_Task18"],
            $params["TS_Time19"],
            $params["TS_Task19"],
            $params["TS_Time20"],
            $params["TS_Task20"],
            $params["TS_Time21"],
            $params["TS_Task21"],
            $params["TS_Time22"],
            $params["TS_Task22"],
            $params["TS_Time23"],
            $params["TS_Task23"],
            $params["TS_Time24"],
            $params["TS_Task24"],
            $params["TS_Time25"],
            $params["TS_Task25"],
            $params["TS_Time26"],
            $params["TS_Task26"],
            $params["TS_Time27"],
            $params["TS_Task27"],
            $params["TS_Time28"],
            $params["TS_Task28"],
            $params["TS_Time29"],
            $params["TS_Task29"],
            $params["TS_Time30"],
            $params["TS_Task30"],
            $params["TS_Time31"],
            $params["TS_Task31"],
            intval($params["id"])));

    }

    $result !== false ? $status = STATUS_SUCCESS : $status = STATUS_FAIL;
    return rest_ensure_response($status);

}