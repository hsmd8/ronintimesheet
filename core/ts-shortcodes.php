<?php

class TS_Shortcodes


{

    function __construct()
    {
        add_shortcode('tslogin', array(&$this, 'tslogin' ));
    }


    function tslogin()
    {
        global $user_login;
        $output = '';

        // In case of a login error.
        if ( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) :
            $output = "<div class='aa_error'><p>" . LABEL_LOGIN_ERROR . "</p></div>";
        endif;

        if ( is_user_logged_in() ) :

            echo LABEL_HELLO . " " . wp_get_current_user()->display_name;

        else:

            $args = array(
                'echo' => true,
                'form_id' => 'loginform',
                'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_REQUEST["to"] ? $_REQUEST["to"] : $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                'label_username' => __( 'Username' ),
                'label_password' => __( 'Password' ),
                'label_remember' => __( 'Remember Me' ),
                'label_log_in' => __( 'Log In' ),
                'id_username' => 'user_login',
                'id_password' => 'user_pass',
                'id_remember' => 'rememberme',
                'id_submit' => 'wp-submit',
                'remember' => true,
                'value_username' => NULL,
                'value_remember' => false );
            $output = wp_login_form($args);

        endif;

        return $output;

        }

}