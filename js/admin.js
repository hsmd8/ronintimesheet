function Timesheets($, UI, phpData) {
  "use strict";

  var timesheet = {

    dom: {
      table: {},
      currentDoc : {}
    },

    state : {
      ajaxUrl : phpData.apiSettings.root + phpData.apiSettings.namespace
    },

    init : function() {

      timesheet.dom.table = $(UI.listAllContainer).DataTable({});

    },

    clearTable : function() {
      timesheet.dom.table
        .rows()
        .remove()
        .draw();
    },

    getLockedInfo : function (id) {
     return '{0} (<i class="timesheetSet" data-item="{1}" data-action="{2}">{3}</i>)'
        .format(
        phpData.labels.LABEL_DONE,
        id,
        phpData.actions.ACTION_UNLOCK,
        phpData.labels.LABEL_REJECT
      );
    },

    getApproveInfo : function (id) {
      return '<i class="timesheetSet" data-item="{0}" data-action="{1}">{2}</i>'
        .format(
        id,
        phpData.actions.ACTION_APPROVE,
        phpData.labels.LABEL_ACCEPT
      );
    },
    getAlreadyApprovedInfo : function (id) {
      return '{0} (<i class="timesheetSet" data-item="{1}" data-action="{2}">{3}</i>)'
        .format(
        phpData.labels.LABEL_PROCESSED,
        id,
        phpData.actions.ACTION_UNAPPROVE,
        phpData.labels.LABEL_UNDO
      );
    },

    getDetailsInfo : function (type, id) {
      return '<i class="timesheetDetails" data-item="{0}" data-level="{1}">{2}</i>'
        .format(
        id,
        type,
        phpData.labels.LABEL_DETAILS
      );
    },

    daysInMonth : function (month,year) {
      return new Date(year, month, 0).getDate();
    },

    getValue : function (param) {
      return param  ? param : "";
    },

    isWeekend : function  (month, year, day) {

      var isWeekend = false;

      var myDate = new Date(year, month, day);

      if(myDate.getDay() == 6 || myDate.getDay() == 0) isWeekend = true;

      return isWeekend;

    },

    getDetailsForBackend : function(ts, id, daysCount) {

      timesheet.dom.currentDoc = ts;

      var table = $('<table id="backendDataTable">'),
        header = $('<div class="details-header">');

      table.append(
        '<thead><tr><td>{0}</td><td>{1}</td><td>{2}</td></tr></thead><tfoot><th align="right">{3}</th><th></th><th></th></tfoot>'.
          format(
          phpData.labels.LABEL_DATE,
          phpData.labels.LABEL_HOURS_COUNT,
          phpData.labels.LABEL_PROJECT,
          phpData.labels.LABEL_TOTAL
        )
      );

      header.append('<span class="name block">{0} : <i>{1}</i></span> <span class="from">{2} : <i>{3}</i></span> <span class="to">{4} : <i>{5}</i></span> <span class="done block">{6} : <i>{7}</i></span> <span class="processed block">{8} : <i>{9}</i> </span>'
        .format(
        phpData.labels.LABEL_WORKER,
        ts["TS_User"],
        phpData.labels.LABEL_FROM,
        ts["TS_Date1"],
        phpData.labels.LABEL_TO,
        ts["TS_Date"+daysCount],
        phpData.labels.LABEL_DONE,
        ts["TS_Locked"] == 0 ? phpData.labels.LABEL_NO : phpData.labels.LABEL_YES,
        phpData.labels.LABEL_PROCESSED,
        ts["TS_Processed"] == 0 ? phpData.labels.LABEL_NO : phpData.labels.LABEL_YES
      ));

      for (var i=1; i<daysCount+1; i++) {

        var weekend = "";

        if (timesheet.isWeekend((ts["TS_Month"]-1), ts["TS_Year"], i)) weekend = "weekend";

        var row = $('<tr class="{0}">'.format(weekend));

        row.append('<td>{0}</td>'
          .format(ts["TS_Date" + i]))
          .append('<td>{0}</td>'
            .format((timesheet.getValue(ts["TS_Time" + i]) || 0 )))
          .append('<td>{0}</td>'
            .format(timesheet.getValue(ts["TS_Task"+i])));



        table.append(row);

      }

      var pdf = $('<div class="pdf">PDF</div>');

      $(UI.timesheetDetailsContainer).html(table);
      $(UI.timesheetDetailsContainer).prepend(pdf);
      $(UI.timesheetDetailsContainer).prepend(header);
      timesheet.drawDetailsTable('#backendDataTable', 1, false);

      timesheet.pdfHandler();

    },

    getDetailsForFrontend : function (ts, id, daysCount) {


      var table = $('<table id="frontendDataTable">');

      table.append(
        '<thead><tr><td>{0}</td><td>{1}</td><td>{2}</td></tr></thead><tfoot><th align="right">{3}</th><th></th><th></th></tfoot>'.
        format(
          phpData.labels.LABEL_DATE,
          phpData.labels.LABEL_HOURS_COUNT,
          phpData.labels.LABEL_PROJECT,
          phpData.labels.LABEL_TOTAL
        )
      );

      var tbody = $('<tbody>');


      for (var i = 1; i < daysCount + 1; i++) {

        var weekend = "";

        if (timesheet.isWeekend((ts["TS_Month"] - 1), ts["TS_Year"], i)) weekend = "weekend";

        var row = $('<tr class="{0}">'.
          format(weekend));

        row.
          append('<td>{0}</td>'.
            format(ts["TS_Date" + i]))
          .append('<td id="TS_Time{0}" class="time" contenteditable onKeypress="if(event.keyCode < 48 || event.keyCode > 57){return false;}">{1}</td>'
            .format(i, (timesheet.getValue(ts["TS_Time" + i]) || 0 )))
          .append('<td id="TS_Task{0}" class="task" contenteditable>{1}</td>'.
            format(i, timesheet.getValue(ts["TS_Task" + i])));

        tbody.append(row);

      }

      table.append(tbody);
      $(UI.timesheetDetailsContainer).html(table);



      var editable = ts["TS_Locked"] == 0;

      timesheet.drawDetailsTable('#frontendDataTable', 1, editable);

      if (editable) {
        var save = '<i class="timesheetSave" data-item="{0}" data-save="0">{1}</i>'.format(id, phpData.labels.LABEL_SAVE);
        var send = '<i class="timesheetSave" data-item="{0}" data-save="1">{1}</i>'.format(id, phpData.labels.LABEL_SEND);
        $(UI.timesheetDetailsContainer).append(save, send);
      }


    },


    drawDetailsTable : function (container, index, editable) {

      var tab = $(container).DataTable({
        "keys": editable,
        "info": false,
        "paging": false,
        "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api();
          $( api.column( index ).footer() ).html(
            api.column( index ).data().reduce( function ( a, b ) {
              return parseInt(a) + parseInt(b);
            }, 0 )
          );
        }
      });

      if (editable) {

        // keys introduces the key-blur event where we can detect moving off a cell
        tab
          .on('key-blur', function (e, datatable, cell) {
            cell.data($(cell.node()).html()).draw()
          });

      }
    },

    pdfHandler : function() {

      $(UI.pdf).off().on("click", function (event) {

        var daysCount = timesheet.daysInMonth(timesheet.dom.currentDoc["TS_Month"],timesheet.dom.currentDoc["TS_Year"]);

        var data = [],
          headerData = [],
          footerData = [],
          total = 0;


        headerData.push({text: 'Data', style: 'tableHeader'},
          {text: 'Ilość godzin', style: 'tableHeader'},
          {text: 'Projekt', style: 'tableHeader'});

        data.push(headerData);

        for (var i=1; i<daysCount+1; i++) {

          total += parseInt(timesheet.dom.currentDoc["TS_Time" + i]) || 0;
          var dayData = [], color;

          timesheet.isWeekend((timesheet.dom.currentDoc["TS_Month"] - 1), timesheet.dom.currentDoc["TS_Year"], i) ? color = '#dddddd' : color = '#ffffff'

          dayData.push({
            fillColor: color,
            text: timesheet.dom.currentDoc["TS_Date" + i] || ""
          },
            {
              fillColor: color,
              text: timesheet.dom.currentDoc["TS_Time" + i] || 0
            },
            {
              fillColor:color,
              text: timesheet.dom.currentDoc["TS_Task" + i] || ""
            });


          data.push(dayData);

        }



        footerData.push({text: 'Suma', style: 'tableHeader'},
          {text: total, style: 'tableHeader'},
          {text: '', style: 'tableHeader'});

        data.push(footerData);



        var dd = {
          content: [
            {
              columns: [
                {
                  text: 'Zestawienie czasu pracy i wykonanych zadań'
                },
                {
                  text: 'Osoba raportująca : '
                }
              ]
            },
            {
              alignment: 'justify',
              columns: [
                {
                  text: 'w zakresie od : ' + timesheet.dom.currentDoc["TS_Date1"] + ' do : ' + timesheet.dom.currentDoc["TS_Date"+daysCount],
                  margin: [0, 0, 0, 20]
                },
                {
                  text: timesheet.dom.currentDoc["TS_User"],
                  style: 'bold',
                  margin: [0, 0, 0, 20]
                }
              ]
            },
            {
              style: 'tableExample',
              table: {
                widths: ['auto', 'auto', '*'],
                body: data
              },
              layout: 'lightHorizontalLines'
            },
          ],
          styles: {
            bold : {
              bold: true
            },
            header: {
              fontSize: 18,
              bold: true,
              margin: [0, 0, 0, 10]
            },
            subheader: {
              fontSize: 14,
              bold: true,
              margin: [0, 10, 0, 5]
            },
            tableExample: {
              margin: [0, 5, 0, 15]
            },
            tableHeader: {
              bold: true,
              fontSize: 12,
              color: 'black'
            }
          },
          defaultStyle: {
            // alignment: 'justify'
            columnGap: 20
          }

        }
        pdfMake.createPdf(dd).download();

      });

    },

    timesheetDetailsHandler : function() {

      $(UI.timesheetDetails).off().on("click", function (event) {

        var id = $(this).attr("data-item"),
          type = parseInt($(this).attr("data-level")),
          ajaxUrl = timesheet.state.ajaxUrl + '/timesheet-details/';

        $.ajax( {
          type: 'POST',
          url: ajaxUrl,
          data: { 'id': id },
          dataType: 'json',
          beforeSend: function ( xhr ) {
            // sending nonce in order to be able to get user data in api callback
            xhr.setRequestHeader( 'X-WP-Nonce', phpData.apiSettings.nonce );
          }
        } ).done( function( data, status, xhr ) {

          var ts = data[0];
          var id = ts["id"];

          var daysCount = timesheet.daysInMonth(ts["TS_Month"],ts["TS_Year"]);

          if (type === 1) {
            timesheet.getDetailsForBackend(ts, id, daysCount);

          } else {
            timesheet.getDetailsForFrontend(ts, id, daysCount)
          }

          var close = '<div id="timesheet-details-close">X</div>';
          $(UI.timesheetDetailsContainer).prepend(close);

          timesheet.timesheetSaveHandler();
          timesheet.timesheetCloseHandler();

        } ).fail( function( xhr, status, error ) {
          var $e = $( '<div class="ajax-error"></div>' ).text( error.message );
          $(UI.timesheetDetailsContainer).after( $e );
        } );


      });
    },

    timesheetCloseHandler : function() {
      $(UI.timesheetDetailsClose).off().on("click", function(){
        $(UI.timesheetDetailsContainer).html("");

      });

    },

    timesheetLockerHandler : function() {

      $(UI.timesheetSet).off().on("click", function (event) {

        var id = $(this).attr("data-item"),
          action = $(this).attr("data-action"),
          ajaxUrl = timesheet.state.ajaxUrl + '/timesheet-locker/';

        $.ajax( {
          type: 'POST',
          url: ajaxUrl,
          data: {
            'id': id,
            'action' : action
          },
          beforeSend: function ( xhr ) {
            // sending nonce in order to be able to get user data in api callback
            xhr.setRequestHeader( 'X-WP-Nonce', phpData.apiSettings.nonce );
          },
          dataType: 'json'
        } ).done( function( data, status, xhr ) {

          var locked, approved;

          if (data === phpData.status.STATUS_SUCCESS) {

            locked = $("#result-{0} > td:eq(4) ".format(id));
            approved = $("#result-{0} > td:eq(6) ".format(id));

            switch (action) {
              case phpData.actions.ACTION_LOCK :
                $(locked).html(timesheet.getLockedInfo(id));
                break;
              case phpData.actions.ACTION_UNLOCK :
                $(locked).html(phpData.labels.LABEL_NOT_DONE);
                $(approved).html("");
                break;
              case phpData.actions.ACTION_APPROVE :
                $(approved).html(timesheet.getAlreadyApprovedInfo(id));
                break;
              case phpData.actions.ACTION_UNAPPROVE :
                $(approved).html(timesheet.getApproveInfo(id));
                break;
            }
          }

          // have to re-hook handlers for newly added elements
          timesheet.timesheetLockerHandler();

        } ).fail( function( xhr, status, error ) {
          // do nothing
        } );

      });

    },

    timesheetSaveHandler : function() {

      $(UI.timesheetSave).off().on("click", function (event) {

        var id = $(this).attr("data-item"),
          save = $(this).attr("data-save"),
          ajaxUrl = timesheet.state.ajaxUrl + '/timesheet-update/';


        var output = {};

        var table = $(UI.frontendDataTable);

        var values = table.find('.time, .task');

        $(values).each(function(){
          output[$(this).attr('id')] = $(this).html();
        });

        output["id"] = id;
        output["lock"] = save;


        $.ajax( {
          type: 'POST',
          url: ajaxUrl,
          data: output,
          dataType: 'json',
          beforeSend: function ( xhr ) {
            xhr.setRequestHeader( 'X-WP-Nonce', phpData.apiSettings.nonce );
          }
        } ).done( function( data, status, xhr ) {
          if (data === phpData.status.STATUS_SUCCESS) {
            $(UI.detailsContainer).html(phpData.labels.LABEL_SAVE_SUCCESS);
          } else {
            var $e = $( '<div class="ajax-error"></div>' ).text( phpData.labels.LABEL_ERROR );
            $(UI.detailsContainer).append( $e );
          }
        } ).fail( function( xhr, status, error ) {
            var $e = $( '<div class="ajax-error"></div>' ).text( error.message );
            $(UI.detailsContainer).append( $e );

        } );
      });

    },

    filterHandler : function() {

      $(UI.filterForm).off().on("submit", function (event) {

        if (typeof window.FormData !== 'function') {
          return;
        }

        var formData = new FormData( $(this).get( 0 ) );

        var ajaxUrl = timesheet.state.ajaxUrl + '/timesheet-list/';

        $.post( {
          type: 'POST',
          url: ajaxUrl,
          data: formData,
          dataType: 'json',
          processData: false,
          contentType: false
        } ).done( function( data, status, xhr ) {


          timesheet.clearTable();


          $(data).each(function() {

            var id = $(this).attr("id");


            var approve, locked;
            if ($(this).attr("TS_Locked") == 1) {
              locked = timesheet.getLockedInfo(id);
              if ($(this).attr("TS_Processed") == 0) {
                approve = timesheet.getApproveInfo(id);
              } else {
                approve = timesheet.getAlreadyApprovedInfo(id);
              }
            } else {
              approve = '';
              locked = phpData.labels.LABEL_NOT_DONE;
            }


            var details = timesheet.getDetailsInfo(1,id);
            var row = timesheet.dom.table
              .row.add( [
                $(this).attr("id"),
                $(this).attr("TS_User"),
                $(this).attr("TS_Month"),
                $(this).attr("TS_Year"),
                locked,
                details,
                approve
              ]).
              draw().node();

            $(row).attr("id", "result-" + $(this).attr("id"));

            timesheet.timesheetDetailsHandler();
            timesheet.timesheetLockerHandler();

          });


        } ).fail( function( xhr, status, error ) {
          var $e = $( '<div class="ajax-error"></div>' ).text( error.message );
          $(UI.filterForm).after( $e );
        } );

        event.preventDefault();


      });

    },

    stringPrototype : function() {
      if (!String.prototype.format) {
        String.prototype.format = function() {
          var args = arguments;
          return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
              ;
          });
        };
      }
    }

  };

  // helper for text formating
  timesheet.stringPrototype();

  // datatables initialization
  timesheet.init();

  // starter handlers
  timesheet.filterHandler();
  timesheet.timesheetDetailsHandler();
  timesheet.timesheetLockerHandler();


}

(function ($) {
  "use strict";

  $(document).ready(function () {

    phpData = $.extend( {
      cached: 0
    }, phpData );

    var UI = {

      listAllContainer: '#listResults',
      detailsContainer : '#timesheet-details',
      filterForm : '#filterResults',
      timesheetDetails : '.timesheetDetails',
      timesheetDetailsContainer : "#timesheet-details",
      timesheetSet : '.timesheetSet',
      timesheetSave : '.timesheetSave',
      frontendDataTable : '#frontendDataTable',
      backendDataTable : '#backendDataTable',
      timesheetDetailsClose : '#timesheet-details-close',
      pdf : '.pdf'


  };

    Timesheets($, UI, phpData);
  });

})(jQuery);
