<?php

if(!current_user_can('manage_options'))
{
    die('Access Denied');
}

global $wpdb;

$table_name = $wpdb->prefix . "ts_timesheets";

$results=$wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE id>%d",0));


echo '<div id="timesheet-details"></div>';
echo '<form id="filterResults" method="POST" action="#">';

do_action('ts_build_years_menu');
do_action('ts_build_months_menu');
do_action ('ts_build_users_menu');

echo '<input type="submit" name="filter" value="'.LABEL_FILTER.'">';

echo '</form>';



echo '<table id="listResults" class="table table-striped table-bordered" cellspacing="0" width="100%">';

echo '<thead><tr>
        <td>'.LABEL_ID.'</td>
        <td>'.LABEL_WORKER.'</td>
        <td>'.LABEL_MONTH.'</td>
        <td>'.LABEL_YEAR.'</td>
        <td>'.LABEL_LOCKED.'</td>
        <td>'.LABEL_DETAILS.'</td>
        <td>'.LABEL_ACCEPT.'</td></tr></thead>';

echo '<tbody>';


if (count($results) > 0) {

    foreach ($results as $result)  {

        $approve = $locked = "";

        if ($result->TS_Locked == 1) {
            $locked = locked($result->id);
            if ($result->TS_Processed == 0) {
                $approve = doApprove($result->id);
              } else {
                $approve = alreadyApproved($result->id);
            }
        } else {
            $locked = LABEL_NOT_DONE;
        }


        ?>


        <tr id="result-<?php echo $result->id; ?>">
            <td><?php echo $result->id; ?></td>
            <td><?php echo get_userdata($result->TS_User)->display_name ?></td>
            <td><?php echo  date_i18n('F', mktime(0,0,0,$result->TS_Month)); ?></td>
            <td><?php echo $result->TS_Year; ?></td>
            <td><?php echo $locked; ?></td>
            <td><i class="timesheetDetails" data-level="1" data-item="<?php echo $result->id; ?>"><?php echo LABEL_DETAILS; ?></i></td>
            <td><?php echo $approve; ?></td>
        </tr>

    <?php
    }



}
echo '</tbody>';
echo '</table>';










