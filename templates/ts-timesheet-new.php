<?php



echo '<form method="POST" >';

do_action('ts_build_years_menu');
do_action('ts_build_months_menu');
do_action ('ts_build_users_menu');


echo '<input type="submit" name="submit" value="'.LABEL_ADD.'">';

echo '</form>';


if(isset($_POST['submit']))
{
    addTimeSheetToDatabase($_POST);
}

function addTimeSheetToDatabase($data) {

    global $wpdb;

    $dates = getDates($data["year"], $data["month"]);

    $table_name = $wpdb->prefix . "ts_timesheets";


    if (validateUser($data["user"], $data["year"], $data["month"])) {

        $wpdb->insert(
            $table_name,
            array_merge(array(
                'TS_Month' => $data["month"],
                'TS_Year' => $data["year"],
                'TS_User' => $data["user"],
                'TS_Locked' => false,
                'TS_Processed' => false
            ), $dates)
        );

    } else {
        echo LABEL_TIMESHEET_ALREADY_EXISTS;
    }



}

function validateUser($user, $year, $month) {

    global $wpdb;

    $canSetUp = false;

    $table_name = $wpdb->prefix . "ts_timesheets";

    $resultsCount=$wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE TS_User = %s AND TS_Year = %d AND TS_Month = %s", $user, $year, $month));

    if(count($resultsCount)==0) {
        $canSetUp = true;
    }

    return $canSetUp;
}

function getDates ($year, $month) {

    $dates = array();

    for($d=1; $d<=31; $d++)
    {
        $time=mktime(12, 0, 0, $month, $d, $year);
        if (date('m', $time)==$month) {
            $dates['TS_Date'.$d] = date('Y-m-d', $time);
        }
    }

    return $dates;

}